package clipperlib
{
	import clipperlib.flascc.clipPolygon;
	import clipperlib.flascc.CModule;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author
	 */
	public class Test extends Sprite
	{
		public function Test()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		
		private function onAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			tf = new TextField();
			tf.autoSize = "left";
			addChild(tf);
			
			init();
			
			sv = new <int>[100, 100, 300, 100, 300, 300, 10, 300];
			cv = new <int>[100, 0, 400, 400, 50, 300];
			
			drawVerts(sv, 0xff0000, 0.1);
			drawVerts(cv, 0x0000ff, 0.1);
			
			var s:Vector.<Vector.<int>> = Clipper.clipPolygon(sv, cv, ClipType.Difference);
			if (s != null)
			{
				tf.text = s.length.toString();
				for (var i:int = 0; i < s.length; i++)
				{
					drawVerts(s[i], 0x44DB9E, 0.5);
				}
			}
			
			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
		}
		
		private var _toggle:Boolean;
		private var _runs:int;
		private var _timeCount:int;
		private function onKeyDown(e:KeyboardEvent):void 
		{
			_toggle = !_toggle;
			
			if (_toggle)
			{
				_runs = 0;
				_timeCount = 0;
				stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			else
			{
				tf.appendText("\n" + (_timeCount / _runs));
				stage.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
		}
		
		private function onEnterFrame(e:Event):void 
		{
			var beforeTime:int = getTimer();
			for (var i:int = 0; i < 100; i++)
			{
				Clipper.clipPolygon(sv, cv, ClipType.Difference);
			}
			var afterTime:int = getTimer();
			_timeCount += afterTime-beforeTime;
			_runs++;
			
			if ((_runs % 30) == 0)
			{
				tf.text = "\n" + (_timeCount / _runs);
				_timeCount = _runs = 0;
			}
		}
		
		private function drawVerts(v:Vector.<int>, color:uint, alpha:Number):void
		{
			graphics.beginFill(color, alpha);
			graphics.lineStyle(2, color);
			
			graphics.moveTo(v[0], v[1]);
			for (var i:int = 2; i < v.length; i += 2)
				graphics.lineTo(v[i], v[i + 1]);
			
			graphics.endFill();
		}
		
		private static var _inited:Boolean;
		private var tf:TextField;
		private var sv:Vector.<int>;
		private var cv:Vector.<int>;
		
		public static function clipPolygon(subj:Array, clip:Array, clipType:int):void
		{
			if (!_inited)
				init();
		
			//var resultCount:int = clipperlib.flascc.clipPolygon(subj, clip, clipType);
		}
		
		static private function init():void
		{
			CModule.startAsync();
			_inited = true;
		}
	
	}

}