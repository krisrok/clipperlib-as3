package clipperlib 
{
	/**
	 * ...
	 * @author 
	 */
	public class ClipType 
	{
		public static const Intersect:int = 0;
		public static const Union:int = 1;
		public static const Difference:int = 2;
		public static const XOR:int = 3;
	}

}