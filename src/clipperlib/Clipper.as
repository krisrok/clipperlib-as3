package clipperlib 
{
	import clipperlib.flascc.CModule;
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class Clipper 
	{
		
		private static var _inited:Boolean;

		private static function init():void
		{
			CModule.startAsync();
			_inited = true;
		}
		
		public static function clipPolygonFromPoints(subject:Vector.<Point>, clip:Vector.<Point>, clipType:int):Vector.<Vector.<Point>>
		{
			return null;
		}
		
		public static function clipPolygon(subject:Vector.<int>, clip:Vector.<int>, clipType:int):Vector.<Vector.<int>>
		{
			if (!_inited)
				init();
			
			var solutions:Vector.<Vector.<int>> = null;
			
			var subjPtr:int = CModule.malloc(4 * clip.length);
			CModule.writeIntVector(subjPtr, clip);
			
			var clipPtr:int = CModule.malloc(4 * subject.length);
			CModule.writeIntVector(clipPtr, subject);
			
			var resultPtr:int = clipperlib.flascc.clipPolygon(subjPtr, clip.length, clipPtr, subject.length, clipType);
			
			if (resultPtr != 0)
			{
				var result:Vector.<int> = CModule.readIntVector(resultPtr, 2);
				
				if (result[0] > 0)
				{
					solutions = new Vector.<Vector.<int>>();
					
					var soluPathsPtr:Vector.<int> = CModule.readIntVector(result[1], result[0]);
					for (var i:int = 0; i < result[0]; i++)
					{
						var soluPath:Vector.<int> = CModule.readIntVector(soluPathsPtr[i] + 4, CModule.read32(soluPathsPtr[i]));
						CModule.free(soluPathsPtr[i]);
						
						solutions.push(soluPath);
					}
				}
				CModule.free(resultPtr);
				CModule.free(result[1]);
			}
			
			CModule.free(subjPtr);
			CModule.free(clipPtr);
			
			return solutions;
		}
		
	}

}