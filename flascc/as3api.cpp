#include "clipper.cpp"
#include "AS3/AS3.h"

using namespace ClipperLib;
using namespace std;

Clipper _c;

void clipPolygon() __attribute((used,
	annotate("as3sig:public function clipPolygon(_subjPtr:int, _subjLen:int, _clipPtr:int, _clipLen:int, _clipType:int):int"),
	annotate("as3package:clipperlib.flascc")));

void clipPolygon()
{
	int* subjPtr;
	int subjLen;
	AS3_GetScalarFromVar(subjPtr, _subjPtr);
	AS3_GetScalarFromVar(subjLen, _subjLen);
	
	int* clipPtr;
	int clipLen;
	AS3_GetScalarFromVar(clipPtr, _clipPtr);
	AS3_GetScalarFromVar(clipLen, _clipLen);
	
	int clipType;
	AS3_GetScalarFromVar(clipType, _clipType);
	
	Paths subjPaths(1), clipPaths(1), soluPaths;
	
	for(int i = 0; i < subjLen; i+=2)
	{
		subjPaths[0].push_back(IntPoint(subjPtr[i], subjPtr[i+1]));
	}
	
	for(int i = 0; i < clipLen; i+=2)
	{
		clipPaths[0].push_back(IntPoint(clipPtr[i], clipPtr[i+1]));
	}
	
	_c.Clear();
	_c.AddPaths(subjPaths, ptSubject, true);
	_c.AddPaths(clipPaths, ptClip, true);
	if(!_c.Execute((ClipType)clipType, soluPaths, pftNonZero, pftNonZero))
		AS3_Return(0);
	
	int soluPathsSize = soluPaths.size();
	int** soluPtr = (int**)malloc(sizeof(int) * soluPathsSize);
	for(int i = 0; i < soluPathsSize; i++)
	{
		int soluPathSize = soluPaths[i].size();
		soluPtr[i] = (int*)malloc(sizeof(int) * (soluPathSize * 2 + 1));
		soluPtr[i][0] = soluPathSize * 2;
		for(int k = 0; k < soluPathSize; k++)
		{
			soluPtr[i][k*2+1] = soluPaths[i][k].X;
			soluPtr[i][k*2+2] = soluPaths[i][k].Y;
		}
	}
	
	int result[] = { soluPathsSize, (int)soluPtr };
	
	AS3_Return(result);
}



int main()
{
	// We still need a main function for the SWC. this function must be called
	// so that all the static init code is executed before any library functions
	// are used.
	//
	// The main function for a library must throw an exception so that it does
	// not return normally. Returning normally would cause the static
	// destructors to be executed leaving the library in an unuseable state.

	AS3_GoAsync();
}

	/*
	AS3_Val asSubjectVertices;
	AS3_Val asClipVertices;
	int subjectVerticeCount, clipVerticeCount, clipTypeArg, subjectFillTypeArg, clipFillTypeArg;
	// Get the function arguments (subjectVertices:Array, subjectVerticeCount:int, extractVertices:Array,
	//	extractVerticeCount:int, clipType:int, subjectFillType:int, clipFillType:int)
	AS3_ArrayValue(
			args,
			"AS3ValType, IntType, AS3ValType, IntType, IntType, IntType, IntType",
			&asSubjectVertices, &subjectVerticeCount, &asClipVertices, &clipVerticeCount,
			&clipTypeArg, &subjectFillTypeArg, &clipFillTypeArg
	);

	Polygon subjectPolygon(subjectVerticeCount / 2), clipPolygon(clipVerticeCount / 2);
	Polygons solution;

	// Populate the subject polygon
	for (int i = 0; i < subjectVerticeCount; i += 2) {
		subjectPolygon[i / 2] = IntPoint(
			AS3_IntValue(AS3_Get( asSubjectVertices, AS3_Int(i) )),
			AS3_IntValue(AS3_Get( asSubjectVertices, AS3_Int(i+1) ))
		);
	}

	// Populate the clip polygon
	for (int i = 0; i < clipVerticeCount; i += 2) {
		clipPolygon[i / 2] = IntPoint(
			AS3_IntValue(AS3_Get( asClipVertices, AS3_Int(i) )),
			AS3_IntValue(AS3_Get( asClipVertices, AS3_Int(i+1) ))
		);
	}

	// Create the AS3 return array
	AS3_Val returnArray = AS3_Array("");

	ClipType clipType;
	switch (clipTypeArg) {
		default:
		case 0: clipType = ctIntersection; break;
		case 1: clipType = ctUnion; break;
		case 2: clipType = ctDifference; break;
		case 3: clipType = ctXor; break;
	}

	PolyFillType subjectFillType, clipFillType;
	switch (subjectFillTypeArg) {
		default:
		case 0: subjectFillType = pftEvenOdd; break;
		case 1: subjectFillType = pftNonZero; break;
	}
	switch (clipFillTypeArg) {
		default:
		case 0: clipFillType = pftEvenOdd; break;
		case 1: clipFillType = pftNonZero; break;
	}



	Clipper c;
	c.AddPolygon(subjectPolygon, ptSubject);
	c.AddPolygon(clipPolygon, ptClip);
	if (c.Execute(clipType, solution, subjectFillType, clipFillType)) {
		for (int i = 0; i < (int)solution.size(); i++) {
			// Create a new AS3 array
			AS3_Val verticeArray = AS3_Array("");

			for (int j = 0; j < (int)solution[i].size(); j++) {
				// Push all the vertices into the array
				AS3_Set(verticeArray, AS3_Int(j * 2), AS3_Int(solution[i][j].X));
				AS3_Set(verticeArray, AS3_Int(j * 2 + 1), AS3_Int(solution[i][j].Y));
			}
			// Insert the array into the returnArray
			AS3_Set(returnArray, AS3_Int(i), verticeArray);
		}
	}

	// Cleanup
	AS3_Release(asSubjectVertices);
	AS3_Release(asClipVertices);

	return returnArray;
}

int main(void) {
	//define the methods exposed to ActionScript
	//typed as an ActionScript Function instance
	AS3_Val clipPolygonMethod = AS3_Function( NULL, clipPolygon );

	// construct an object that holds references to the functions
	AS3_Val result = AS3_Object( "clipPolygon: AS3ValType", clipPolygonMethod );

	// Release
	AS3_Release( clipPolygonMethod );

	// notify that we initialized -- THIS DOES NOT RETURN!
	AS3_LibInit( result );

	// should never get here!
	return 0;
}
*/